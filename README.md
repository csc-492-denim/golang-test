# Golang Test

Test project to experiment with Go/Rosie and verify that a Go environment is set up correctly.

## Installing Go and Rosie (Ubuntu/debian)

```bash
sudo ./install.sh
```
