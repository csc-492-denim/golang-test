#!/bin/bash

set -eu

VERSION=1.9.3
OS=linux
ARCH=amd64
apt-get update
apt-get install wget tar git make gcc libreadline6 libreadline6-dev

if [ ! -d /usr/local/go ]; then
    wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz
    tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz
    rm -f go$VERSION.$OS-$ARCH.tar.gz
fi

grep '/usr/local/go/bin' ~/.profile || echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.profile

. ~/.profile

if ! which rosie; then
    git clone https://github.com/jamiejennings/rosie-pattern-language.git
    pushd rosie-pattern-language
    make LUADEBUG=1
    make test
    make install
    popd
    rm -rf rosie-pattern-language
fi

echo "SUCCESS!"
