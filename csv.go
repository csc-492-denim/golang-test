package main

import (
	"encoding/csv"
	"fmt"
	"os"

	"./rosie-pattern-language/src/librosie/go/src/rosie"
)

func main() {
	fmt.Println("Here we Go!")
	f, err := os.Open("test.csv")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	defer f.Close()
	r := csv.NewReader(f)
	data, err := r.ReadAll()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	fmt.Println(data)

	engine, err := rosie.New("My Engine")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	cfg, err := engine.Config()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	fmt.Println(cfg)

	ok, pkgName, messages, err := engine.ImportPkg("all")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	fmt.Println(ok, pkgName, messages)

	pat, messages, err := engine.Compile("all.things")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	fmt.Println(pat, messages)

	f2, err := os.Open("test.csv")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	defer f2.Close()
	dataBytes := make([]byte, 1000)
	n, err := f2.Read(dataBytes)
	fmt.Println("bytes read", n)
	fmt.Println(dataBytes)
	match, err := pat.Match(dataBytes[:n])
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	fmt.Println(match.Data)
}
